/*
 * BANCO DE DADOS - SISTEMA DE GESTÃO ESCOLAR (SGE)
 */
package br.ba.sge.model.dao;

import br.ba.sge.connection.ConnectionFactory;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import br.ba.sge.model.bean.Pessoa;
/**
 * CRUD - Creat | Read | Update | Delete
 * @author Nando_Cezar
 */
public class PessoaDAO {
    
    public void Create(Pessoa pessoa) throws Exception{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        
        try {           
            String sql = "INSERT INTO tb_pessoa (pes_nome, pes_endereco, pes_uf, pes_telefone, pes_cpf, pes_email, pes_cargo) "+ "values (?, ?, ?, ?, ?, ?, ?)";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, pessoa.getPes_nome());
            stmt.setString(2, pessoa.getPes_endereco());
            stmt.setString(3, pessoa.getPes_uf());
            stmt.setString(4, pessoa.getPes_telefone());
            stmt.setString(5, pessoa.getPes_cpf());
            stmt.setString(6, pessoa.getPes_email());
            stmt.setString(7, pessoa.getPes_cargo());
         
            stmt.executeUpdate();   
            
            JOptionPane.showMessageDialog(null, "Salvo com sucesso!!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }
    
    public List<Pessoa> Read() throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Pessoa> pessoas = new ArrayList<>(); /*Lista pessoas vai armazenar valores que estão em ResultSet*/
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM tb_pessoa");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Pessoa pessoa = new Pessoa();
                
                pessoa.setPes_codigo  (rs.getInt("pes_codigo"));
                pessoa.setPes_nome    (rs.getString("pes_nome"));               
                pessoa.setPes_telefone(rs.getString("pes_telefone"));
                pessoa.setPes_cpf     (rs.getString("pes_cpf"));
                pessoa.setPes_email   (rs.getString("pes_email"));
                
                pessoas.add(pessoa); /*Armazenando dados da tabela 'pessoa' para lista 'pessoas'*/
            }
            
        }catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }        
        return pessoas;
    }   
    
    public List<Pessoa> ReadForNome(String nome) throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Pessoa> pessoas = new ArrayList<>(); /*Lista pessoas vai armazenar valores que estão em ResultSet*/
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM tb_pessoa WHERE pes_nome LIKE ?");
            stmt.setString(1, "%"+nome+"%");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Pessoa pessoa = new Pessoa();
                
                pessoa.setPes_codigo  (rs.getInt("pes_codigo"));
                pessoa.setPes_nome    (rs.getString("pes_nome"));               
                pessoa.setPes_telefone(rs.getString("pes_telefone"));
                pessoa.setPes_cpf     (rs.getString("pes_cpf"));
                pessoa.setPes_email   (rs.getString("pes_email"));
                
                pessoas.add(pessoa); /*Armazenando dados da tabela 'pessoa' para lista 'pessoas'*/
            }
            
        }catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }        
        return pessoas;
    }   
    
      public List<Pessoa> ReadForProfessor() throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Pessoa> pessoas = new ArrayList<>(); /*Lista pessoas vai armazenar valores que estão em ResultSet*/
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM tb_pessoa WHERE pes_cargo LIKE ?");
            stmt.setString(1, "%Professor%");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Pessoa pessoa = new Pessoa();
                
                pessoa.setPes_codigo  (rs.getInt("pes_codigo"));
                pessoa.setPes_nome    (rs.getString("pes_nome"));               
                pessoa.setPes_telefone(rs.getString("pes_telefone"));
                pessoa.setPes_cpf     (rs.getString("pes_cpf"));
                pessoa.setPes_email   (rs.getString("pes_email"));
                
                pessoas.add(pessoa); /*Armazenando dados da tabela 'pessoa' para lista 'pessoas'*/
            }
            
        }catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }        
        return pessoas;
    } 
      
      public List<Pessoa> ReadForAluno() throws SQLException{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        List<Pessoa> pessoas = new ArrayList<>(); /*Lista pessoas vai armazenar valores que estão em ResultSet*/
        
        try{
            
            stmt = conn.prepareStatement("SELECT * FROM tb_pessoa WHERE pes_cargo LIKE ?");
            stmt.setString(1, "%Aluno%");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Pessoa pessoa = new Pessoa();
                
                pessoa.setPes_codigo  (rs.getInt("pes_codigo"));
                pessoa.setPes_nome    (rs.getString("pes_nome"));               
                pessoa.setPes_telefone(rs.getString("pes_telefone"));
                pessoa.setPes_cpf     (rs.getString("pes_cpf"));
                pessoa.setPes_email   (rs.getString("pes_email"));
                
                pessoas.add(pessoa); /*Armazenando dados da tabela 'pessoa' para lista 'pessoas'*/
            }
            
        }catch (SQLException ex){
            JOptionPane.showMessageDialog(null, "Erro ao Salvar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt, rs);
        }        
        return pessoas;
    }   
    
    public void Update(Pessoa pessoa) throws Exception{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        
        try {           
            String sql = "UPDATE tb_pessoa SET pes_nome = ?, pes_telefone = ?, pes_cpf = ?, pes_email = ? WHERE pes_codigo = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, pessoa.getPes_nome());
            stmt.setString(2, pessoa.getPes_telefone());
            stmt.setString(3, pessoa.getPes_cpf());
            stmt.setString(4, pessoa.getPes_email());
            stmt.setInt   (5, pessoa.getPes_codigo());
         
            stmt.executeUpdate();   
            
            JOptionPane.showMessageDialog(null, "Atualizado com sucesso!!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Atualizar: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }
    
    public void Delete(Pessoa pessoa) throws Exception{
        
        Connection conn = ConnectionFactory.getConnection( );
        PreparedStatement stmt = null;
        
        try {           
            String sql = "DELETE FROM tb_pessoa WHERE pes_codigo = ?";
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, pessoa.getPes_codigo());
         
            stmt.executeUpdate();   
            
            JOptionPane.showMessageDialog(null, "Excluído com sucesso!!!");
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao Excluir: "+ex);
        }finally{
            ConnectionFactory.closeConnection(conn, stmt);
        }
    }
}       