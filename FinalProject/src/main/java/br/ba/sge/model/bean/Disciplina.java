/*
 * BANCO DE DADOS - SISTEMA DE GESTÃO ESCOLAR (SGE)
 */
package br.ba.sge.model.bean;
/**
 *
 * @author Nando_Cezar
 */
public class Disciplina {
    
    private int    disc_codigo;
    private String disc_nome;
    private int    disc_cargaHoraria;
    private Pessoa disc_prof_pes_codigo;
    private int    disc_limiteAlunos;

    public Disciplina() {}

    public Disciplina(int disc_codigo, String disc_nome, int disc_cargaHoraria, Pessoa disc_prof_pes_codigo, int disc_limiteAlunos) {
        this.disc_codigo = disc_codigo;
        this.disc_nome = disc_nome;
        this.disc_cargaHoraria = disc_cargaHoraria;
        this.disc_prof_pes_codigo = disc_prof_pes_codigo;
        this.disc_limiteAlunos = disc_limiteAlunos;
    }

    public int getDisc_codigo() {
        return disc_codigo;
    }

    public void setDisc_codigo(int disc_codigo) {
        this.disc_codigo = disc_codigo;
    }

    public String getDisc_nome() {
        return disc_nome;
    }

    public void setDisc_nome(String disc_nome) {
        this.disc_nome = disc_nome;
    }

    public int getDisc_cargaHoraria() {
        return disc_cargaHoraria;
    }

    public void setDisc_cargaHoraria(int disc_cargaHoraria) {
        this.disc_cargaHoraria = disc_cargaHoraria;
    }

    public Pessoa getDisc_prof_pes_codigo() {
        return disc_prof_pes_codigo;
    }

    public void setDisc_prof_pes_codigo(Pessoa disc_prof_pes_codigo) {
        this.disc_prof_pes_codigo = disc_prof_pes_codigo;
    }

    public int getDisc_LimiteAlunos() {
        return disc_limiteAlunos;
    }

    public void setDisc_LimiteAlunos(int disc_limiteAlunos) {
        this.disc_limiteAlunos = disc_limiteAlunos;
    }

    @Override
    public String toString() {
        return getDisc_nome(); //To change body of generated methods, choose Tools | Templates.
    }

    
}
