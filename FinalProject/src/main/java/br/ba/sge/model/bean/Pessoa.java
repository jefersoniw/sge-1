/*
 * BANCO DE DADOS - SISTEMA DE GESTÃO ESCOLAR (SGE)
 */
package br.ba.sge.model.bean;
/**
 *
 * @author Nando_Cezar
 */
public class Pessoa {
    
    private int    pes_codigo;
    private String pes_nome;
    private String pes_endereco;
    private String pes_uf;
    private String pes_telefone;
    private String pes_cpf;
    private String pes_email;
    private String pes_cargo; /*adicional*/

    public Pessoa() {}
    
    public Pessoa(int pes_codigo, String pes_nome, String pes_endereco, String pes_uf, String pes_telefone, String pes_cpf, String pes_email, String pes_cargo) {
        this.pes_codigo = pes_codigo;
        this.pes_nome = pes_nome;
        this.pes_endereco = pes_endereco;
        this.pes_uf = pes_uf;
        this.pes_telefone = pes_telefone;
        this.pes_cpf = pes_cpf;
        this.pes_email = pes_email;
        this.pes_cargo = pes_cargo;

    }
    
    

    public int getPes_codigo() {
        return pes_codigo;
    }

    public void setPes_codigo(int pes_codigo) {
        this.pes_codigo = pes_codigo;
    }

    public String getPes_nome() {
        return pes_nome;
    }

    public void setPes_nome(String pes_nome) {
        this.pes_nome = pes_nome;
    }

    public String getPes_endereco() {
        return pes_endereco;
    }

    public void setPes_endereco(String pes_endereco) {
        this.pes_endereco = pes_endereco;
    }

    public String getPes_uf() {
        return pes_uf;
    }

    public void setPes_uf(String pes_uf) {
        this.pes_uf = pes_uf;
    }

    public String getPes_telefone() {
        return pes_telefone;
    }

    public void setPes_telefone(String pes_telefone) {
        this.pes_telefone = pes_telefone;
    }

    public String getPes_cpf() {
        return pes_cpf;
    }

    public void setPes_cpf(String pes_cpf) {
        this.pes_cpf = pes_cpf;
    }

    public String getPes_email() {
        return pes_email;
    }

    public void setPes_email(String pes_email) {
        this.pes_email = pes_email;
    }
    
    public String getPes_cargo() {
        return pes_cargo;
    }

    public void setPes_cargo(String pes_cargo) {
        this.pes_cargo = pes_cargo;
    }

    @Override
    public String toString() {
        return getPes_nome(); 
    }
   
}

